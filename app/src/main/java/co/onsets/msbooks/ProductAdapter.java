package co.onsets.msbooks;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.onsets.msbooks.Model.Book;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    public List<Book> books;
    private ItemClickListener clickListener;
    private int categoryPosition = 0;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, price;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvName);
            price = view.findViewById(R.id.tvPrice);
            view.setTag(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public class Heading extends RecyclerView.ViewHolder {
        public TextView category;

        public Heading(View view) {
            super(view);
            category = view.findViewById(R.id.tvCategory);
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if(categoryPosition == 0){
            return position % 2 * 2;
        }
        else{
            return 0;
        }
    }

    public ProductAdapter(List<Book> books, int position) {
        this.books = books;
        this.categoryPosition = position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case 0:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row_list, parent, false);
                MyViewHolder holder2 = new MyViewHolder(itemView);
                holder = holder2;
                break;
            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_heading_row_list, parent, false);
                Heading holder1 = new Heading(itemView);
                holder = holder1;
                break;
        }
        return holder;
    }

    /*@Override
    public void onBindViewHolder(ProductAdapter.MyViewHolder holder, int position) {

        Book book = books.get(position);
        holder.name.setText(book.getName());
        holder.price.setText(String.format("%d", book.getPrice()));
    }*/

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                MyViewHolder viewHolder0 = (MyViewHolder)holder;
                Book book = books.get(position);
                viewHolder0.name.setText(book.getName());
                viewHolder0.price.setText(String.format("%d", book.getPrice()));
                break;

            case 2:
                Heading viewHolder2 = (Heading)holder;
                viewHolder2.category.setText("Category");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
