package co.onsets.msbooks;

import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import co.onsets.msbooks.Model.TeacherSchedule;

public class SchedulesTeacherViewFragment extends Fragment implements ItemClickListener {
    private List<TeacherSchedule> schedules = new ArrayList<>();
    private RecyclerView recyclerView;
    SwipeController swipeController = null;
    private ScheduleAdapter mAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_teacher_view_schedules, null);
        Button btnAddNew = v.findViewById(R.id.btnAddNew);
        btnAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new AddScheduleFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        recyclerView = v.findViewById(R.id.my_recycler_view);
        mAdapter = new ScheduleAdapter(getActivity(), schedules);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
        prepareScheduleData();

        //Swipe
        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                mAdapter.schedules.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
            }
            @Override
            public void onLeftClicked(int position) {
                mAdapter.schedules.remove(position);
                mAdapter.notifyItemRemoved(position);
                mAdapter.notifyItemRangeChanged(position, mAdapter.getItemCount());
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });
        return v;
    }

    @Override
    public void onClick(View view, int position) {
        Fragment newFragment = new AddScheduleFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void prepareScheduleData() {
        TeacherSchedule schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        mAdapter.notifyDataSetChanged();
    }
}
