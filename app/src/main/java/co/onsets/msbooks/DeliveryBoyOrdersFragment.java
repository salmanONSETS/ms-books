package co.onsets.msbooks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import co.onsets.msbooks.Model.Order;

public class DeliveryBoyOrdersFragment extends Fragment implements ItemClickListener {
    private RecyclerView recyclerView;
    private DeliveryBoyOrderAdapter mAdapter;
    private List<Order> orders = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_delivery_boy_orders, null);
        recyclerView = v.findViewById(R.id.my_recycler_view);
        mAdapter = new DeliveryBoyOrderAdapter(orders);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
        prepareOrderData();
        return v;
    }

    @Override
    public void onClick(View view, int position) {
        Fragment newFragment = new ViewOrderFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void prepareOrderData() {
        Order order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore, Punjab, Pakistan", "0300", "0300", "Salman", "Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        order = new Order(1, "10-12-19", "1000", "Cash on Delivery", "Gulberg 3, Lahore", "0300", "0300", "Salman","Active");
        orders.add(order);

        mAdapter.notifyDataSetChanged();
    }
}
