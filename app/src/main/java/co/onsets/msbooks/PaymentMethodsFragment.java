package co.onsets.msbooks;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PaymentMethodsFragment extends Fragment implements View.OnClickListener {
    CheckBox cardCheckbox, cashOnDeliveryCheckbox;
    LinearLayout llCard;
    Button btnConfirm;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_payment_methods, null);
        cardCheckbox = v.findViewById(R.id.checkbox_card);
        cashOnDeliveryCheckbox = v.findViewById(R.id.checkbox_cashOnDelivery);
        cardCheckbox.setOnClickListener(this);
        cashOnDeliveryCheckbox.setOnClickListener(this);
        llCard = v.findViewById(R.id.llcardPayment);
        btnConfirm = v.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custom_dialog);

                // set the custom dialog components - text, image and button
                TextView tvMessage = dialog.findViewById(R.id.tvMessage);
                TextView tvMessage1 = dialog.findViewById(R.id.tvMessage1);
                TextView tvMessage2 = dialog.findViewById(R.id.tvMessage2);
                tvMessage.setText(R.string.thanksMessage);
                tvMessage1.setText(R.string.thanksMessage1);
                tvMessage2.setText(R.string.thanksMessage2);
                Button dialogButton =  dialog.findViewById(R.id.btnViewOrder);
                dialogButton.setText(R.string.viewOrder);
                dialogButton.setVisibility(View.VISIBLE);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Fragment newFragment = new ViewOrderFragment();
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container, newFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                });

                dialog.show();
            }
        });
        return v;
    }


    @Override
    public void onClick(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkbox_card:
                if (checked) {
                    llCard.setVisibility(View.VISIBLE);
                    if (cashOnDeliveryCheckbox.isChecked()){
                        cashOnDeliveryCheckbox.setChecked(false);
                    }
                }
                else{
                    llCard.setVisibility(View.GONE);
                }
                break;
            case R.id.checkbox_cashOnDelivery:
                if (checked){
                    if (cardCheckbox.isChecked()){
                        cardCheckbox.setChecked(false);
                    }
                    llCard.setVisibility(View.GONE);
                }
                else{
                }
                break;
            // TODO: Veggie sandwich
        }
    }
    public void openDialog() {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

        // Set Message
        TextView msg = new TextView(getActivity());
        // Message Properties
        msg.setText(R.string.thanksMessage);
        msg.setPadding(20, 20, 20, 20);   // Set Position
        msg.setGravity(Gravity.CENTER);
        msg.setTextColor(Color.BLACK);
        alertDialog.setView(msg);

        // Set Button
        // you can more buttons
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"View Order", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Perform Action on Button
            }
        });

        new Dialog(getActivity());
        alertDialog.show();

        // Set Properties for OK Button
        final Button okBT = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        LinearLayout.LayoutParams neutralBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        neutralBtnLP.gravity = Gravity.CENTER_HORIZONTAL;
        okBT.setPadding(10, 10, 10, 10);   // Set Position
        okBT.setTextColor(Color.WHITE);
        okBT.setBackgroundResource(R.drawable.custom_button);
        okBT.setLayoutParams(neutralBtnLP);
    }

}
