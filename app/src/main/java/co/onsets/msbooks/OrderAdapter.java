package co.onsets.msbooks;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.onsets.msbooks.Model.Order;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
    public List<Order> orders;
    private ItemClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTotalAmount, tvOrderDate, tvPaymentMode;

        public MyViewHolder(View view) {
            super(view);
            tvTotalAmount = view.findViewById(R.id.tvTotalAmountValue);
            tvOrderDate = view.findViewById(R.id.tvOrderDateValue);
            tvPaymentMode = view.findViewById(R.id.tvPaymentModeValue);
            view.setTag(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public OrderAdapter(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Order order = orders.get(position);
        holder.tvTotalAmount.setText(order.getTotalAmount());
        holder.tvPaymentMode.setText(order.getPaymentMode());
        holder.tvOrderDate.setText(order.getOrderDate());
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
