package co.onsets.msbooks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import co.onsets.msbooks.Model.Book;

public class CustomPastPapersFragment extends Fragment implements ItemClickListener {
    Spinner spSubjects, spClasses, spVariants, spPapers, spYears, spRangeFrom, spRangeTo;
    private List<Book> books = new ArrayList<>();
    private RecyclerView recyclerView;
    private BooksAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_custom_past_papers, null);
        spClasses = v.findViewById(R.id.spClass);
        spSubjects = v.findViewById(R.id.spSubject);
        spPapers = v.findViewById(R.id.spPapers);
        spVariants = v.findViewById(R.id.spVariants);
        spRangeFrom = v.findViewById(R.id.spRangeFrom);
        spRangeTo = v.findViewById(R.id.spRangeTo);
        spYears = v.findViewById(R.id.spYear);

        recyclerView = v.findViewById(R.id.my_recycler_view);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.classes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spClasses.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.subjects, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSubjects.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.variants, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVariants.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.papers, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPapers.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.fromArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRangeFrom.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.toArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRangeTo.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.years, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYears.setAdapter(adapter);

        mAdapter = new BooksAdapter(books);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
        prepareBookData();
        return v;
    }

    @Override
    public void onClick(View view, int position) {
        Fragment newFragment = new ProductDetailsFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void prepareBookData() {
        Book book = new Book(1, "Book 1", 100, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "A good book");
        books.add(book);

        book = new Book(1, "Book 1", 100, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        books.add(book);

        mAdapter.notifyDataSetChanged();
    }
}
