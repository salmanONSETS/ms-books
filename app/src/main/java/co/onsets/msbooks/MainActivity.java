package co.onsets.msbooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(new HomeFragment());
        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Fragment newFragment = null;

                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        if(menuItem.getTitle().toString().contentEquals("Cart") ){
                            newFragment = new CartFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Search")){
                            newFragment = new SearchResultsFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Wishlist")){
                            newFragment = new WishlistFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Home")){
                            newFragment = new HomeFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Order History")){
                            newFragment = new OrderListFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Appointments")){
                            newFragment = new AppointmentsFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Contact Us")){
                            newFragment = new ContactUsFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("App Manual")){
                            newFragment = new CustomPastPapersFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("ShopKeeper")){
                            Intent i = new Intent(MainActivity.this, ShopkeeperActivity.class);
                            startActivity(i);
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Delivery Boy")){
                            Intent i = new Intent(MainActivity.this, DeliveryBoyActivity.class);
                            startActivity(i);
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Teacher")){
                            Intent i = new Intent(MainActivity.this, TeacherActivity.class);
                            startActivity(i);
                        }
                        loadFragment(newFragment);
                        return true;
                    }
                });

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // parent.getItemAtPosition(pos)

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new HomeFragment();
                break;

            case R.id.navigation_cart:
                fragment = new CartFragment();
                break;

            case R.id.navigation_drawer:
                fragment = new TeacherResourceFragment();
                //drawerLayout.openDrawer(GravityCompat.START);
                break;

            case R.id.navigation_favourite:
                fragment = new WishlistFragment();
                break;

            case R.id.navigation_search:
                fragment = new SearchResultsFragment();
                break;
        }

        return loadFragment(fragment);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    break;

                case R.id.navigation_cart:
                    fragment = new CartFragment();
                    break;

                case R.id.navigation_drawer:
                    fragment = new TeacherResourceFragment();
                    //drawerLayout.openDrawer(GravityCompat.START);
                    break;

                case R.id.navigation_favourite:
                    fragment = new WishlistFragment();
                    break;

                case R.id.navigation_search:
                    fragment = new SearchResultsFragment();
                    break;
            }

            return loadFragment(fragment);
        }
    };
}
