package co.onsets.msbooks;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import co.onsets.msbooks.Model.Order;

public class DeliveryBoyOrderAdapter extends RecyclerView.Adapter<DeliveryBoyOrderAdapter.MyViewHolder> {
    public List<Order> orders;
    private ItemClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvOrderCode, tvTotalAmount, tvStatus;

        public MyViewHolder(View view) {
            super(view);
            tvOrderCode = view.findViewById(R.id.tvOrderCode);
            tvTotalAmount = view.findViewById(R.id.tvTotalAmount);
            tvStatus = view.findViewById(R.id.tvStatus);
            view.setTag(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public DeliveryBoyOrderAdapter(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public DeliveryBoyOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_list_row_delivery_boy, parent, false);

        return new DeliveryBoyOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeliveryBoyOrderAdapter.MyViewHolder holder, int position) {
        Order order = orders.get(position);
        holder.tvOrderCode.setText(String.format("%d", order.getId()));
        holder.tvTotalAmount.setText(order.getTotalAmount());
        holder.tvStatus.setText(order.getStatus());
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
