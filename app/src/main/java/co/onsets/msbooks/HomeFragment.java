package co.onsets.msbooks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import co.onsets.msbooks.Model.Category;

public class HomeFragment extends Fragment {
    Category[] categories1;
    String[] categoriesArray;
    ProgressDialog p;
    CustomSpinnerAdapter categoryAdapter;
    Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard

        //
        // Create an ArrayAdapter using the string array and a default spinner layout
        // Specify the layout to use when the list of choices appears
        //
        // Apply the adapter to the spinner

        View v = inflater.inflate(R.layout.fragment_main, null);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        //AsyncTaskExample asyncTask=new AsyncTaskExample();
        String url = "http://localhost:8000/api/getFirstLevelCategories";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new Gson();
                categories1 = gson.fromJson(response.toString(), Category[].class);
                for (int i = 0 ; i < categories1.length ; i++){
                    categoriesArray[i] = categories1[i].getName();
                }
                Log.i("Response", response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonArrayRequest);
        //asyncTask.execute(url);
        spinner = v.findViewById(R.id.spinnerOptionsLevel1);
        TextView btnAdvance = v.findViewById(R.id.btnAdvanceSearch);
        //spinner.setOnItemSelectedListener();
        btnAdvance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new SearchResultsFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        return v;
    }

    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskExample extends AsyncTask<String, String, Category[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*p = new ProgressDialog(getActivity().getApplicationContext());
            p.setMessage("Please wait...It is downloading");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();*/
        }
        @Override
        protected Category[] doInBackground(String...strings) {
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, strings[0], null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Gson gson = new Gson();
                    categories1 = gson.fromJson(response.toString(), Category[].class);
                    for (int i = 0 ; i < categories1.length ; i++){
                        categoriesArray[i] = categories1[i].getName();
                    }
                    Log.i("Response", categoriesArray[0]);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO: Handle error
                }
            });
            return categories1;
        }
        @Override
        protected void onPostExecute(Category[] categories) {
            super.onPostExecute(categories);
            Toast.makeText(getActivity().getApplicationContext(), "Books Loaded", Toast.LENGTH_SHORT).show();
            /*categoryAdapter = new CustomSpinnerAdapter(getActivity().getApplicationContext(),categories1);
            spinner.setAdapter(categoryAdapter);*/
        }
    }
}
