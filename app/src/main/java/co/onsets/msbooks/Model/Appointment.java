package co.onsets.msbooks.Model;

public class Appointment {
    private int id;
    private String teacherName, email, phone, date, time, status;

    public Appointment(int id, String teacherName, String email, String phone, String date, String time, String status) {
        this.id = id;
        this.teacherName = teacherName;
        this.email = email;
        this.phone = phone;
        this.date = date;
        this.time = time;
        this.status = status;
    }

    public Appointment() {
        this.id = 0;
        this.teacherName = "";
        this.email = "";
        this.phone = "";
        this.date = "";
        this.time = "";
        this.status = "";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
