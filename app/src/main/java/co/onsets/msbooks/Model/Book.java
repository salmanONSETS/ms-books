package co.onsets.msbooks.Model;

public class Book {
    private String name, description;
    private int id, price;

    public Book(){
        this.description = "";
        this.id = -1;
        this.name = "";
        this.price = 0;
    }

    public Book(int id, String name, int price, String description){
        this.price = price;
        this.name = name;
        this.description = description;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
