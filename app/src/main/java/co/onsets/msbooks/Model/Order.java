package co.onsets.msbooks.Model;

public class Order {
    private int id;
    private String orderDate, totalAmount, paymentMode, address, alternativeContact, contact, name, status;

    public Order(int id, String orderDate, String totalAmount, String paymentMode, String address, String alternativeContact, String contact, String name, String status) {
        this.id = id;
        this.orderDate = orderDate;
        this.totalAmount = totalAmount;
        this.paymentMode = paymentMode;
        this.address = address;
        this.alternativeContact = alternativeContact;
        this.contact = contact;
        this.name = name;
        this.status = status;
    }

    public Order() {
        this.id = 0;
        this.status = "";
        this.orderDate = "";
        this.totalAmount = "";
        this.paymentMode = "";
        this.address = "";
        this.alternativeContact = "";
        this.contact = "";
        this.name = "";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAlternativeContact() {
        return alternativeContact;
    }

    public void setAlternativeContact(String alternativeContact) {
        this.alternativeContact = alternativeContact;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
