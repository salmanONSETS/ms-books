package co.onsets.msbooks.Model;

public class TeacherSchedule {
    int id;
    String date, time, location, class1, subject, status;

    public TeacherSchedule(int id, String date, String time, String location, String class1, String subject, String status) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.location = location;
        this.class1 = class1;
        this.subject = subject;
        this.status = status;
    }

    public TeacherSchedule() {
        this.id = 0;
        this.date = "";
        this.time = "";
        this.location = "";
        this.class1 = "";
        this.subject = "";
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
