package co.onsets.msbooks;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.onsets.msbooks.Model.Category;

public class CustomSpinnerAdapter extends ArrayAdapter<Category> {
    private Context mContext;
    private Category[] categories;

    public CustomSpinnerAdapter(@NonNull Context context, Category[] list) {
        super(context, 0 , list);
        mContext = context;
        categories = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.custom_spinner,parent,false);

        Category currentCategory = categories[position];

        TextView name = listItem.findViewById(R.id.tvName);
        name.setText(currentCategory.getName());
        return listItem;
    }
}
