package co.onsets.msbooks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import co.onsets.msbooks.Model.TeacherSchedule;

public class TeacherScheduleFragment extends Fragment implements ItemClickListener {
    private List<TeacherSchedule> schedules = new ArrayList<>();
    private RecyclerView recyclerView;
    private ScheduleAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_teacher_schedule, null);
        recyclerView = v.findViewById(R.id.my_recycler_view);
        mAdapter = new ScheduleAdapter(getActivity(), schedules);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
        prepareScheduleData();
        return v;
    }

    @Override
    public void onClick(View view, int position) {

    }

    private void prepareScheduleData() {
        TeacherSchedule schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        schedule = new TeacherSchedule(1, "15-03-19", "10:00-11:00", "Lahore", "A-Level", "Math", "Send Request");
        schedules.add(schedule);

        mAdapter.notifyDataSetChanged();
    }
}
