package co.onsets.msbooks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import co.onsets.msbooks.Model.Appointment;

public class AppointmentsFragment extends Fragment implements ItemClickListener {
    private List<Appointment> appointments = new ArrayList<>();
    private RecyclerView recyclerView;
    private AppointmentAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_appointments, null);
        recyclerView = v.findViewById(R.id.my_recycler_view);
        mAdapter = new AppointmentAdapter(appointments);
        Spinner spinner = v.findViewById(R.id.spTimeFilter);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.appointmentsTimeArray, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);
        prepareAppointmentData();
        return v;
    }

    @Override
    public void onClick(View view, int position) {
    }

    private void prepareAppointmentData() {
        Appointment appointment = new Appointment(1, "Dr. Salman Sajid", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Badar", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Aqsa", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Dawood", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Mughees", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        appointment = new Appointment(1, "Salman", "salman@gmail.com", "0300", "15-03-19", "10:00 - 11:00", "Accepted");
        appointments.add(appointment);

        mAdapter.notifyDataSetChanged();
    }
}
