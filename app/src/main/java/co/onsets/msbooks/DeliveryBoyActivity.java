package co.onsets.msbooks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

public class DeliveryBoyActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_boy);

        loadFragment(new DeliveryBoyOrdersFragment());
        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        Fragment newFragment = null;

                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        if(menuItem.getTitle().toString().contentEquals("Orders")){
                            newFragment = new DeliveryBoyOrdersFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Order History")){
                            newFragment = new DeliveryBoyOrdersFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Picked Orders")){
                            newFragment = new DeliveryBoyOrdersFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("Contact Us")){
                            newFragment = new ContactUsFragment();
                        }
                        else if(menuItem.getTitle().toString().contentEquals("App Manual")){
                            newFragment = new CustomPastPapersFragment();
                        }
                        loadFragment(newFragment);
                        return true;
                    }
                });

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // parent.getItemAtPosition(pos)

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new DeliveryBoyOrdersFragment();
                break;
            case R.id.navigation_drawer:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
        }

        return loadFragment(fragment);
    }
}
