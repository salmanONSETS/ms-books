package co.onsets.msbooks;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.onsets.msbooks.ItemClickListener;
import co.onsets.msbooks.Model.Appointment;
import co.onsets.msbooks.R;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.MyViewHolder> {
    public List<Appointment> appointments;
    private ItemClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView teacherName, phone, email, time, date, status;

        public MyViewHolder(View view) {
            super(view);
            teacherName = view.findViewById(R.id.tvTeacherName);
            phone = view.findViewById(R.id.tvPhone);
            email = view.findViewById(R.id.tvEmail);
            time = view.findViewById(R.id.tvTime);
            date = view.findViewById(R.id.tvDate);
            status = view.findViewById(R.id.tvStatus);
            view.setTag(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public AppointmentAdapter(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    @Override
    public AppointmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointments_list_row, parent, false);

        return new AppointmentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AppointmentAdapter.MyViewHolder holder, int position) {
        Appointment appointment = appointments.get(position);
        holder.teacherName.setText(appointment.getTeacherName());
        holder.email.setText(appointment.getEmail());
        holder.phone.setText(appointment.getPhone());
        holder.date.setText(appointment.getDate());
        holder.time.setText(appointment.getTime());
        holder.status.setText(appointment.getStatus());
    }

    @Override
    public int getItemCount() {
        return appointments.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
