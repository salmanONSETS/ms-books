package co.onsets.msbooks;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import co.onsets.msbooks.Model.TeacherSchedule;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.MyViewHolder> {
    public List<TeacherSchedule> schedules;
    private ItemClickListener clickListener;
    public FragmentActivity context;
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvTime, tvDate, tvClass, tvSubject, tvLocation, tvStatus;
        public MyViewHolder(View view) {
            super(view);
            tvTime = view.findViewById(R.id.tvTime);
            tvDate = view.findViewById(R.id.tvDate);
            tvClass = view.findViewById(R.id.tvClass);
            tvSubject = view.findViewById(R.id.tvSubject);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.custom_dialog);

                    // set the custom dialog components - text, image and button
                    TextView tvMessage = dialog.findViewById(R.id.tvMessage);
                    tvMessage.setText(R.string.appointmentRequestMessage);
                    dialog.show();
                }
            });
            view.setTag(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public ScheduleAdapter(FragmentActivity context, List<TeacherSchedule> schedules) {
        this.schedules = schedules;
        this.context = context;
    }

    @Override
    public ScheduleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedule_list_row, parent, false);

        return new ScheduleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ScheduleAdapter.MyViewHolder holder, int position) {
        TeacherSchedule schedule = schedules.get(position);
        holder.tvTime.setText(schedule.getTime());
        holder.tvDate.setText(schedule.getDate());
        holder.tvClass.setText(schedule.getClass1());
        holder.tvSubject.setText(schedule.getSubject());
        holder.tvLocation.setText(schedule.getLocation());
        holder.tvStatus.setText(schedule.getStatus());
    }

    @Override
    public int getItemCount() {
        return schedules.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
