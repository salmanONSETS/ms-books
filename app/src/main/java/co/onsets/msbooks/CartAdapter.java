package co.onsets.msbooks;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import co.onsets.msbooks.Model.Book;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    public List<Book> books;
    private ItemClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, price;
        public EditText etQuantity;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvName);
            etQuantity = view.findViewById(R.id.etQuantity);
            price = view.findViewById(R.id.tvPrice);
            view.setTag(view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }

    public CartAdapter(List<Book> books) {
        this.books = books;
    }

    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_cart_list_row, parent, false);

        return new CartAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CartAdapter.MyViewHolder holder, int position) {
        Book book = books.get(position);
        holder.name.setText(book.getName());
        holder.etQuantity.setText("1");
        holder.price.setText(String.format("%d", book.getPrice()));
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}
